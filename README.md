# proyecto-intr-ing-grupo1
**Introduccion**
- quien no conoce a la gran cantidad de personas con una mala conexión y/o señal de internet?, al parecer todo el mundo, ya que varios de nosotros hemos tenido al típico amigo o vecino que la señal del internet no le llega muy bien, ya sea por que esta en su pieza y el router esta en otro lado de la casa, o bien la misma infraestructura del hogar limita la señal haciendo que esta no se expanda correctamente. A mas de uno le a pasado esto, o bien nosotros somo ese amigo de la señal débil. Para solucionar Todos estos problemas llega el Repetidor Wi-fi, el cual amplia la señal haciendo que esta llegue a lugar que antes era de dificil acceso, poniendo fin al problema de la baja señal en los rincones del hogar.

> Hardware  requerido

    Raspberry Pi 4 de 2GB RAM
    Tajeta Micro SD 16 GB
    Otro dispositivo de almacenamiento adicional (Disco duro un 1TB o mas, (recomendado))




> Software requerido

    Sistema operativo Rasbian

_**Tutorial**_
- para empezar lo primerop que hicimos fue actualizar nuestro sistema operativo con el siguiente comnado: 
  `sudo apt-get update`

- Despues de esto instalaremos Network manager, el cual le dara las funciones de Hostpot a nuesta Rasperry, los  comandos son los siguientes:  `sudo apt install network-manager network-manager-gnome openvpn \openvpn-systemd-resolved network-manager-openvpn \network-manager-openvpn-gnome`   | `sudo apt purge openresolv dhcpcd5`  | `sudo ln -sf /lib/systemd/resolv.conf /etc/resolv.conf`

- Luego nos iremos al apartado de ethernet y daremos click en edit connections

![](imagen_1.png)

- Nos aparecera una ventada como esta, daremos click donde dice "Add"

![](imagen_2.png)

- Al dar click aparecera este recuadro en el cual tendremos que buscar el tipo de red,  en este caso Wi-Fi, luego de esto le daremos en "create"

![](imagen_3.png)

- en esta ventana seleccionaremos el nombre local que tendra nuestra red el cual se colocla en "Connection name" , luego en SSID seleccionaremos el nombre de nuestra red Wi-fi, despues en la opcion "Mode" seleccionaremos en el modo que pondremos nuestra red, en este caso el modo es "HostPot"

![](imagen_4.png)

- luego nos iremos a Wi-Fi security en el cual colocarremos la seguridad  "WPA & WPA2" y una contraseña a elecciion para nuestra red Wi-Fi y le daremos "Save"

![](imagen_5.png)

- Cuando estemos listos con cada uno de los pasos enteriores procederemos a reinciar nuestra Rasperry para que esta asimile los cambios incorporados, cuando ya la reiniciemos revisaremos que este este funcionado, para esto daremos click en el simbolo de ethernet y ya nos deberia salir nuestra nueva red como muestra la imagen.

![](imagen_6.png)

- por ultimo solo nos quedaria conectaros a esta red por nuestro telefono celular y disfurtar de nuestro nuevo hostpot ;)





